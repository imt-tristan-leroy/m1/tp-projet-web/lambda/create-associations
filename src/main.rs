mod association;

use association::create_associations;
use lambda_http::{run, service_fn, Body, Error, Request, Response};
use mongodb::Client;
use serde::Deserialize;
use serde_json::json;

#[derive(Deserialize)]
struct RequestBody {
    name: String,
    description: String,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let body = event.body();
    let s = std::str::from_utf8(&body).expect("invalid utf-8 sequence");
    //Log into Cloudwatch

    //Serialze JSON into struct.
    //If JSON is incorrect, send back 400 with error.
    let item = match serde_json::from_str::<RequestBody>(s) {
        Ok(item) => item,
        Err(err) => {
            let resp = Response::builder()
                .status(400)
                .header("content-type", "application/json")
                .body(err.to_string().into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    let mongo_uri = "mongodb://localhost:27017";
    let mongo_database = "test"; //"kairos";

    let client = Client::with_uri_str(mongo_uri).await?;
    let database = client.database(mongo_database);

    let id = create_associations(database, item.name, item.description).await?;

    let j = serde_json::to_string(&json!({ "id": id }))?;

    let resp = Response::builder()
        .status(201)
        .header("content-type", "application/json")
        .body(j.into())
        .map_err(Box::new)?;

    return Ok(resp);
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
