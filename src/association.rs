use std::str;

use mongodb::{
    bson::{doc, oid::ObjectId},
    error::Error,
    Database,
};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AssociationModel {
    pub _id: ObjectId,
    pub name: String,
    pub description: String,
}

pub async fn create_associations(
    db: Database,
    name: String,
    description: String,
) -> Result<String, Error> {
    let collection = db.collection::<AssociationModel>("associations");

    let association = AssociationModel {
        _id: ObjectId::new(),
        name,
        description,
    };

    collection.insert_one(&association, None).await?;

    Ok(association._id.to_hex())
}
